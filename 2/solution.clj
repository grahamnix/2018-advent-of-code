(let [input (->> "input"
                 (slurp)
                 (clojure.string/split-lines))
      part1 (->> input
                 (map frequencies)
                 (map #(map second %))
                 (map (fn [x] (filter #(#{2 3} %) x)))
                 (map distinct)
                 (flatten)
                 (frequencies)
                 (map second)
                 (apply *))
      part2 (let [count-differences
                  (fn [id1 id2]
                    (let [pairs (map vector id1 id2)
                          matches (map #(apply = %) pairs)]
                      ((frequencies matches) false)))
                  matching-substring
                  (fn [id1 id2]
                    (let [pairs (map vector id1 id2)]
                      (apply str (map #(if (apply = %) (first %)) pairs))))
                  find-near-match
                  (fn [id-coll id1]
                    (reduce
                     (fn [_acc id2]
                       (if (= 1 (count-differences id1 id2))
                         (reduced (matching-substring id1 id2))
                         nil))
                     nil id-coll))]
              (reduce
               (fn [acc id]
                 (if-let [near-match (find-near-match acc id)]
                   (reduced near-match)
                   (conj acc id)))
               #{} input))]
  {:part1 part1 :part2 part2})

(defmacro some-match [coll x]
  (let [comparison (gensym 'comparison)]
    `((apply some-fn
             (map
              #(partial (fn [& ~comparison]
                          (apply = ~comparison)) %)
              ~coll))
      ~x)))

(some-match [2 3] 4)
