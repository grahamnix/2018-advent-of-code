(let [input (->> "input"
                 (slurp)
                 (clojure.string/split-lines)
                 (map #(Integer. %)))
      part1 (reduce + input)
      part2 (->> input
                 (repeat)
                 (flatten)
                 (reductions +)
                 (reduce #(if (% %2) (reduced %2) (conj % %2)) #{}))]
  {:part1 part1 :part2 part2})
