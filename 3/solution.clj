(defn make-request [request-params]
  (let [[id x y width height] request-params
        h-range (range x (+ x width))
        v-range (range y (+ y height))
        coords (set (for [x h-range y v-range] {:x x :y y}))]
    {:id id :coords coords}))

(defn line->request [line]
  (let [line->params
        (comp #(map (fn [x] (Integer. x)) %)
              #(clojure.string/split % #"\s+|,|x"))]
    (-> line
        line->params
        make-request)))

(defn lines->requests [lines]
  (map line->request lines))

(defn file->lines [filename]
  (let [remove-set #{\# \@ \:}]
    (->> filename (slurp) (remove #(remove-set %)) (apply str)
         (clojure.string/split-lines))))

(defn overlaps [requests]
  (last
   (reduce
    (fn [acc request]
      (let [[seen overlaps] acc]
        (list (clojure.set/union seen request)
              (clojure.set/union
               overlaps
               (clojure.set/intersection seen request)))))
    '(#{} #{})
    requests)))

(defn first-allowed [overlaps requests]
  (reduce
   (fn [_ request]
     (if (empty? (clojure.set/intersection
                  (:coords request)
                  overlaps))
       (reduced (:id request))
       nil))
   nil requests))
                 
(let [requests (lines->requests (file->lines "input"))
      overlaps (overlaps (map :coords requests))
      part1 (count overlaps)
      part2 (first-allowed overlaps requests)]
  {:part1 part1 :part2 part2})
